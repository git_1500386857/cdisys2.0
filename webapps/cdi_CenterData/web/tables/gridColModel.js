var baseUrl = "CenterData?sub_code=8A0731CC39614C90A5D474BC17253713&sub_usercode=414A6DB3BBE6419DA3768E6E25127310";
var session_id = "";
var login_id = "";
var winWidth, winHeight;

function init_page() {
    session_id = localStorage.getItem('session_id');
    if (session_id == null)
        session_id = "";
    login_id = localStorage.getItem('login_id');
    if (login_id == null)
        login_id = "";
}

// 获取用户的session_id
function getUrlParam(k) {
    var regExp = new RegExp('([?]|&)' + k + '=([^&]*)(&|$)');
    var result = window.location.href.match(regExp);
    if (result) {
        return decodeURIComponent(result[2]);
    } else {
        return null;
    }
}

function T01_SELSSS_Result(input) {
    data2 = input.T01_SELSSS;
    var s_result = "";
    var error_desc = "";
    for (var key in data2[0]) {
        if (key == 's_result') {
            s_result = data2[0].s_result;
            error_desc = data2[0].error_desc;
        }
    }
    if (s_result != "1") {
        window.location.href = "login.html";
    } else {}
}

//获取用户登录信息
function T01_SELSSS() {
    $.ajax({
        url: baseUrl,
        type: "POST",
        async: false,
        dataType: "JSON",
        data: {
            session_id: session_id,
            param_name: "T01_SELSSS"
        },
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_SELSSS_Result",
        success: function (msg) {},
        error: function (msg) {
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            })
        }
    });
}

Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

function dateFmt(fmt, date) { //author: meizz
    var o = {
        "M+": date.getMonth() + 1, //月份
        "d+": date.getDate(), //日
        "h+": date.getHours(), //小时
        "m+": date.getMinutes(), //分
        "s+": date.getSeconds(), //秒
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度
        "S": date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

function js_decode(s) {
    while (s.indexOf('+') >= 0)
        s = s.replace('+', '%20');
    return s;
}

function s_encode(s_result) {
    return encodeURIComponent(s_result).replace(/%20/g, "+");
}

function s_decode(s_result) {
    return decodeURIComponent(js_decode(s_result));
}

//formatter:function(value,options,row){return new Date(value).Format('yyyy-MM-dd hh:mm:ss');}
//子设备接口信息
var t_child_proc = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '子设备主键',
        name: 'CHILD_ID',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '子设备接口中文名',
        name: 'INF_CN_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '子设备接口英文名',
        name: 'INF_EN_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, options, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }
];

function getWindowSize() {
    //获取窗口宽度
    if (window.innerWidth) { //兼容火狐，谷歌,safari等浏览器
        winWidth = window.innerWidth;
    } else if ((document.body) && (document.body.clientWidth)) { //兼容IE浏览器
        winWidth = document.body.clientWidth;
    }

    //获取窗口高度
    if (window.innerHeight) {
        winHeight = window.innerHeight;
    } else if ((document.body) && (document.body.clientHeight)) {
        winHeight = document.body.clientHeight;
    }
    //	alert(winHeight);
}

function getIP() {
    var a = window.location.href.replace("http://", "");
    var a_ary = a.substring(0, a.indexOf("/")).split(':');
    return a_ary[0];
}

function GUID() {
    this.date = new Date();
    /* 判断是否初始化过，如果初始化过以下代码，则以下代码将不再执行，实际中只执行一次 */
    if (typeof this.newGUID != 'function') {
        /* 生成GUID码 */
        GUID.prototype.newGUID = function () {
            this.date = new Date();
            var guidStr = '';
            sexadecimalDate = this.hexadecimal(this.getGUIDDate(), 16);
            sexadecimalTime = this.hexadecimal(this.getGUIDTime(), 16);
            for (var i = 0; i < 9; i++) {
                guidStr += Math.floor(Math.random() * 16).toString(16);
            }
            guidStr += sexadecimalDate;
            guidStr += sexadecimalTime;
            while (guidStr.length < 32) {
                guidStr += Math.floor(Math.random() * 16).toString(16);
            }
            return this.formatGUID(guidStr);
        }
        /* * 功能：获取当前日期的GUID格式，即8位数的日期：19700101 * 返回值：返回GUID日期格式的字条串 */
        GUID.prototype.getGUIDDate = function () {
            return this.date.getFullYear() + this.addZero(this.date.getMonth() + 1) + this.addZero(this.date.getDay());
        }
        /* * 功能：获取当前时间的GUID格式，即8位数的时间，包括毫秒，毫秒为2位数：12300933 * 返回值：返回GUID日期格式的字条串 */
        GUID.prototype.getGUIDTime = function () {
            return this.addZero(this.date.getHours()) + this.addZero(this.date.getMinutes()) + this.addZero(this.date.getSeconds()) + this.addZero(parseInt(this.date.getMilliseconds() / 10));
        }
        /* * 功能: 为一位数的正整数前面添加0，如果是可以转成非NaN数字的字符串也可以实现 * 参数: 参数表示准备再前面添加0的数字或可以转换成数字的字符串 * 返回值: 如果符合条件，返回添加0后的字条串类型，否则返回自身的字符串 */
        GUID.prototype.addZero = function (num) {
            if (Number(num).toString() != 'NaN' && num >= 0 && num < 10) {
                return '0' + Math.floor(num);
            } else {
                return num.toString();
            }
        }
        /*  * 功能：将y进制的数值，转换为x进制的数值 * 参数：第1个参数表示欲转换的数值；第2个参数表示欲转换的进制；第3个参数可选，表示当前的进制数，如不写则为10 * 返回值：返回转换后的字符串 */
        GUID.prototype.hexadecimal = function (num, x, y) {
            if (y != undefined) {
                return parseInt(num.toString(), y).toString(x);
            } else {
                return parseInt(num.toString()).toString(x);
            }
        }
        /* * 功能：格式化32位的字符串为GUID模式的字符串 * 参数：第1个参数表示32位的字符串 * 返回值：标准GUID格式的字符串 */
        GUID.prototype.formatGUID = function (guidStr) {
            var str1 = guidStr.slice(0, 8),
            str2 = guidStr.slice(8, 12),
            str3 = guidStr.slice(12, 16),
            str4 = guidStr.slice(16, 20),
            str5 = guidStr.slice(20);
            return guidStr.toUpperCase(); //(str1 + str2 + str3 + str4 + str5).toUpperCase();
        }
    }
}

function DateAdd(interval, number, date) {
    switch (interval) {
    case "y": {
            date.setFullYear(date.getFullYear() + number);
            return date;
            break;
        }
    case "q": {
            date.setMonth(date.getMonth() + number * 3);
            return date;
            break;
        }
    case "m ": {
            date.setMonth(date.getMonth() + number);
            return date;
            break;
        }
    case "w ": {
            date.setDate(date.getDate() + number * 7);
            return date;
            break;
        }
    case "d ": {
            date.setDate(date.getDate() + number);
            return date;
            break;
        }
    case "h ": {
            date.setHours(date.getHours() + number);
            return date;
            break;
        }
    case "m ": {
            date.setMinutes(date.getMinutes() + number);
            return date;
            break;
        }
    case "s ": {
            date.setSeconds(date.getSeconds() + number);
            return date;
            break;
        }
    default: {
            date.setDate(d.getDate() + number);
            return date;
            break;
        }
    }
}
