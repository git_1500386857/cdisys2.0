﻿var iTop = 0;
var ileft = 0;
var sel_proc_id = "-1";
var ly_index;

// 编辑提交数据成功
function submit_success()
{
	init();
	$("#doing").empty();
	$("#doing").attr("style","display:none");
	swal("操作成功!","","success");
	$("a.ui-jqdialog-titlebar-close").click();
}

// 接口名称
var mod_t_proc_name;

function T01_sel_t_db_config(input)
{
	var data = input.T01_sel_t_db_config;
	var s_result = "1";
	var error_desc = "";
	for( var key in data[0])
	{
		if(key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if(s_result == "0")
	{
		layer.close(ly_index);
		swal("数据读取失败!","失败原因:" + error_desc,"warning");
		return false;
	}
	var options = ""
	$.each(input.T01_sel_t_db_config,function(i,obj)
	{
		if(i != input.T01_sel_t_db_config.length - 1)
		{
			options += obj.MAIN_ID + ":" + obj.DB_CN_NAME + ";";
		}
		else
		{
			options += obj.MAIN_ID + ":" + obj.DB_CN_NAME;
		}
	});
	mod_t_proc_name = [{
	label : '主键',
	name : 'MAIN_ID',
	width : '30px',
	index : 'MAIN_ID',
	editable : false,
	key : true,
	readOnly : true,
	editrules : {
		required : true
	}
	},{
	label : '子系统主键',
	name : 'DB_ID',
	width : '30px',
	editable : true,
	readOnly : true,
	editrules : true,
	edittype : 'select',
	editoptions : {
		value : options
	}
	},{
	label : '接口中文名称',
	name : 'INF_CN_NAME',
	width : '70px',
	editable : true,
	editrules : true
	},{
	label : '接口英文名称',
	name : 'INF_EN_NAME',
	width : '70px',
	editable : true,
	editrules : true
	},{
	label : 'SQL语句',
	name : 'INF_EN_SQL',
	width : '70px',
	height : '60px',
	editable : true,
	editrules : true,
	edittype : 'textarea',
	editoptions : {
	rows : "8",
	cols : "45"
	}
	},{
	label : '类型',
	name : 'INF_TYPE',
	width : '70px',
	edittype : 'select',
	formatter : 'select',
	editable : true,
	editrules : true,
	editoptions : {
		value : {
		2 : 'SQL语句',
		1 : '存储过程'
		}
	},
	formatoptions : {
		value : {
		2 : 'SQL语句',
		1 : '存储过程'
		}
	}
	},{
	label : '备注',
	name : 'S_DESC',
	width : '70px',
	editable : true,
	editrules : true
	},{
	label : '系统时间',
	name : 'CREATE_DATE',
	width : '70px',
	editable : true,
	editrules : true,
	readOnly : true,
	formatter : function(value,row)
	{
		return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
	},
	editoptions : {
		defaultValue : new Date().Format('yyyy-MM-dd hh:mm:ss')
	}
	},{
	label : '输入参数拦截器类名',
	name : 'REFLECT_IN_CLASS',
	width : '70px',
	editable : true,
	editrules : true
	},{
	label : '返回结果拦截器类名',
	name : 'REFLECT_OUT_CLASS',
	width : '70px',
	editable : true,
	editrules : true
	}
	];
	layer.close(ly_index);
	init();
}

// 接口输入参数
var mod_t_proc_inparam = [{
label : '主键',
name : 'MAIN_ID',
width : '30px',
index : 'MAIN_ID',
editable : false,
key : true,
readOnly : true,
editrules : {
	required : true
}
},{
label : '接口输入参数中文名称',
name : 'PARAM_CN_NAME',
width : '70px',
editable : true,
editrules : true
},{
label : '接口输入参数英文名称',
name : 'PARAM_EN_NAME',
width : '70px',
editable : true,
editrules : true
},{
label : '接口输入参数值类型',
name : 'PARAM_TYPE',
width : '60px',
editable : true,
editrules : true,
edittype : 'select',
formatter : 'select',
editable : true,
editrules : true,
editoptions : {
	value : {
	INT : 'INT',
	STRING : 'STRING',
	FLOAT : 'FLOAT',
	DATE : 'DATE',
	LIST : 'LIST',
	LIKESTRING : 'LIKESTRING'
	}
},
formatoptions : {
	value : {
	INT : 'INT',
	STRING : 'STRING',
	FLOAT : 'FLOAT',
	DATE : 'DATE',
	LIST : 'LIST',
	LIKESTRING : 'LIKESTRING'
	}
}
},{
label : '接口输入参数大小',
name : 'PARAM_SIZE',
width : '60px',
editable : true,
editrules : true
},{
label : '备注',
name : 'S_DESC',
width : '70px',
editable : true,
editrules : true
},{
label : '系统时间',
name : 'CREATE_DATE',
width : '70px',
editable : true,
editrules : true,
readOnly : true,
formatter : function(value,row)
{
	return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
},
editoptions : {
	defaultValue : new Date().Format('yyyy-MM-dd hh:mm:ss')
}
}
];

// 接口输出参数
var mod_t_proc_outparam = [{
label : '主键',
name : 'MAIN_ID',
width : '30px',
index : 'MAIN_ID',
editable : false,
key : true,
readOnly : true,
editrules : {
	required : true
}
},{
label : '接口输出参数中文名称',
name : 'PARAM_CN_NAME',
width : '70px',
editable : true,
editrules : true
},{
label : '接口输出参数英文名称',
name : 'PARAM_EN_NAME',
width : '70px',
editable : true,
editrules : true
},{
label : '接口输出参数值类型',
name : 'PARAM_TYPE',
width : '60px',
editable : true,
editrules : true,
edittype : 'select',
formatter : 'select',
editoptions : {
	value : {
	INT : 'INT',
	STRING : 'STRING',
	FLOAT : 'FLOAT',
	DATE : 'DATE',
	CURSOR : 'CURSOR'
	}
},
formatoptions : {
	value : {
	INT : 'INT',
	STRING : 'STRING',
	FLOAT : 'FLOAT',
	DATE : 'DATE',
	CURSOR : 'CURSOR'
	}
}
},{
label : '接口输出参数大小',
name : 'PARAM_SIZE',
width : '60px',
editable : true,
editrules : true
},{
label : '备注',
name : 'S_DESC',
width : '70px',
editable : true,
editrules : true
},{
label : '系统时间',
name : 'CREATE_DATE',
width : '70px',
editable : true,
editrules : true,
readOnly : true,
formatter : function(value,row)
{
	return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
},
editoptions : {
	defaultValue : new Date().Format('yyyy-MM-dd hh:mm:ss')
}
}
];

// 接口返回值
var mod_t_proc_return = [{
label : '主键',
name : 'MAIN_ID',
width : '30px',
index : 'MAIN_ID',
editable : false,
key : true,
readOnly : true,
editrules : {
	required : true
}
},{
label : '接口返回值名称',
name : 'RETURN_NAME',
width : '50px',
editable : true,
editrules : true
},{
label : '接口返回值类型',
name : 'RETURN_TYPE',
width : '50px',
edittype : 'select',
formatter : 'select',
editable : true,
editrules : true,
editoptions : {
	value : {
	INT : 'INT',
	STRING : 'STRING',
	FLOAT : 'FLOAT',
	DATE : 'DATE'
	}
}
},{
label : '备注',
name : 'S_DESC',
width : '60px',
editable : true,
editrules : true
},{
label : '是否图片或路径',
name : 'IS_IMG',
width : '50px',
edittype : 'select',
formatter : 'select',
editable : true,
editrules : true,
editoptions : {
	value : {
	0 : '否',
	1 : '是'
	}
}
},{
label : '是否URL编码',
name : 'IS_URLENCODE',
width : '50px',
edittype : 'select',
formatter : 'select',
editable : true,
editrules : true,
editoptions : {
	value : {
	0 : '否',
	1 : '是'
	}
}
},{
label : '是否隐藏List列',
name : 'LIST_HIDDEN',
width : '50px',
edittype : 'select',
formatter : 'select',
editable : true,
editrules : true,
editoptions : {
	value : {
	0 : '否',
	1 : '是'
	}
}
},{
label : '系统时间',
name : 'CREATE_DATE',
width : '70px',
editable : true,
editrules : true,
readOnly : true,
formatter : function(value,row)
{
	return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
},
editoptions : {
	defaultValue : new Date().Format('yyyy-MM-dd hh:mm:ss')
}
}
];

// 综合应用平台信息
var mod_t_sub_sys = [{
label : '主键',
name : 'MAIN_ID',
width : '30px',
index : 'MAIN_ID',
editable : false,
key : true,
readOnly : true,
editrules : {
	required : true
}
},{
label : '综合平台名称',
name : 'SUB_NAME',
width : '70px',
editable : true,
editrules : true
},{
label : '综合平台授权码',
name : 'SUB_CODE',
width : '70px',
editable : true,
editrules : true,
editoptions : {
	defaultValue : new GUID().newGUID()
}
},{
label : '使用有效期',
name : 'LIMIT_DATE',
width : '70px',
editable : true,
editrules : true,
readOnly : true,
formatter : function(value,row)
{
	return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
},
editoptions : {
	defaultValue : DateAdd("y",30,new Date()).Format('yyyy-MM-dd hh:mm:ss')
}
},{
label : '备注',
name : 'S_DESC',
width : '70px',
editable : true,
editrules : true
},{
label : '系统时间',
name : 'CREATE_DATE',
width : '70px',
editable : true,
editrules : true,
readOnly : true,
formatter : function(value,row)
{
	return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
},
editoptions : {
	defaultValue : new Date().Format('yyyy-MM-dd hh:mm:ss')
}
}
];

// 平台用户信息
var mod_t_sub_user = [{
label : '主键',
name : 'MAIN_ID',
width : '30px',
index : 'MAIN_ID',
editable : false,
key : true,
readOnly : true,
editrules : {
	required : true
}
},{
label : '授权用户名称',
name : 'SUB_USERNAME',
width : '30px',
editable : true,
editrules : true
},{
label : '用户授权码',
name : 'SUB_USERCODE',
width : '70px',
editable : true,
editrules : true,
editoptions : {
	defaultValue : new GUID().newGUID()
}
},{
label : '使用有效期',
name : 'LIMIT_DATE',
width : '70px',
editable : true,
editrules : true,
readOnly : true,
formatter : function(value,row)
{
	return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
},
editoptions : {
	defaultValue : new Date().Format('yyyy-MM-dd hh:mm:ss')
}
},{
label : '备注',
name : 'S_DESC',
width : '70px',
editable : true,
editrules : true
},{
label : '系统时间',
name : 'CREATE_DATE',
width : '70px',
editable : true,
editrules : true,
readOnly : true,
formatter : function(value,row)
{
	return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
},
editoptions : {
	defaultValue : new Date().Format('yyyy-MM-dd hh:mm:ss')
}
}
];

// 获取子系统
function get_T01_sel_t_db_config()
{
	ly_index = layer.load();
	$.ajax({
	type : "POST",
	async : false,
	url : baseUrl,
	data : {
	param_name : "T01_sel_t_db_config",
	session_id : session_id
	},
	// 跨域请求的URL
	dataType : "jsonp",
	jsonp : "jsoncallback",
	jsonpCallback : "T01_sel_t_db_config",
	success : function(data2)
	{
	},
	error : function()
	{
		layer.close(ly_index);
		swal({
		title : "告警",
		text : "网络异常或系统故障，请刷新页面！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "刷新",
		closeOnConfirm : false
		},function()
		{
			window.location.reload();
		})
	},
	// 请求完成后的回调函数 (请求成功或失败之后均调用)
	complete : function(XMLHttpRequest,textStatus)
	{
		$("#doing").empty();
		$("#doing").attr("style","display:none");
	}
	});
}

// 获取平台用户信息
function get_T01_sel_t_sub_user_ByPID(sel_sub_id)
{
	ly_index = layer.load();
	$.ajax({
	type : "POST",
	async : false,
	url : baseUrl,
	data : {
	param_name : "T01_sel_t_sub_user_ByPID",
	param_value1 : sel_sub_id,
	session_id : session_id
	},
	// 跨域请求的URL
	dataType : "jsonp",
	jsonp : "jsoncallback",
	jsonpCallback : "T01_sel_t_sub_user_ByPID",
	success : function()
	{
	},
	error : function()
	{
		layer.close(ly_index);
		swal({
		title : "告警",
		text : "网络异常或系统故障，请刷新页面！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "刷新",
		closeOnConfirm : false
		},function()
		{
			layer.close(ly_index);
			window.location.reload();
		})
	},
	// 请求完成后的回调函数 (请求成功或失败之后均调用)
	complete : function(XMLHttpRequest,textStatus)
	{
		$("#doing").empty();
		$("#doing").attr("style","display:none");
	}
	});
}

// 平台用户信息结果
function T01_sel_t_sub_user_ByPID(input)
{
	data = input.T01_sel_t_sub_user_ByPID;
	var s_result = "1";
	var error_desc = "";
	for( var key in data[0])
	{
		if(key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if(s_result == "0")
	{
		layer.close(ly_index);
		swal("数据读取失败!","失败原因:" + error_desc,"warning");
		return false;
	}
	var total = $("#table_list_6").jqGrid('getGridParam','records');
	$("#table_list_6").jqGrid("clearGridData");
	if(typeof (total) == "undefined") // || total == 0)
	{
		$("#table_list_6").jqGrid({
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_sub_user,
		editurl : 'clientArray',
		altRows : false,
		pager : "#pager_list_6",
		viewrecords : true,
		caption : "平台用户列表(sub_usercode=)"
		});
	}
	else
	{
		$("#table_list_6").jqGrid('setGridParam',{
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_sub_user,
		editurl : 'clientArray',
		altRows : true,
		pager : "#pager_list_6",
		viewrecords : true,
		caption : "平台用户列表(sub_usercode=)"
		}).trigger("reloadGrid");
	}
	layer.close(ly_index);
}

// 获取应用平台信息
function get_T01_sel_t_sub_sys()
{
	ly_index = layer.load();
	$.ajax({
	type : "POST",
	async : false,
	url : baseUrl,
	data : {
	param_name : "T01_sel_t_sub_sys",
	session_id : session_id
	},
	// 跨域请求的URL
	dataType : "jsonp",
	jsonp : "jsoncallback",
	jsonpCallback : "T01_sel_t_sub_sys",
	success : function(data2)
	{
	},
	error : function()
	{
		layer.close(ly_index);
		swal({
		title : "告警",
		text : "网络异常或系统故障，请刷新页面！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "刷新",
		closeOnConfirm : false
		},function()
		{
			window.location.reload();
		})
	},
	// 请求完成后的回调函数 (请求成功或失败之后均调用)
	complete : function(XMLHttpRequest,textStatus)
	{
		$("#doing").empty();
		$("#doing").attr("style","display:none");
	}
	});
}

// 应用平台结果
function T01_sel_t_sub_sys(input)
{
	data = input.T01_sel_t_sub_sys;
	var s_result = "1";
	var error_desc = "";
	for( var key in data[0])
	{
		if(key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if(s_result == "0")
	{
		layer.close(ly_index);
		swal("数据读取失败!","失败原因:" + error_desc,"warning");
		return false;
	}
	$("#table_list_5").jqGrid("clearGridData");
	var total = $("#table_list_5").jqGrid('getGridParam','records');
	if(typeof (total) == "undefined") // || total == 0)
	{
		$("#table_list_5").jqGrid({
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_sub_sys,
		editurl : 'clientArray',
		altRows : false,
		pager : "#pager_list_5",
		viewrecords : true,
		caption : "应用平台列表(sub_code=)",
		onSelectRow : function(rowid)
		{
			var sel_sub_id = $('#table_list_5').jqGrid('getRowData',rowid).MAIN_ID;
			get_T01_sel_t_sub_user_ByPID(sel_sub_id);
		}
		});
	}
	else
	{
		$("#table_list_5").jqGrid('setGridParam',{
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_sub_sys,
		editurl : 'clientArray',
		altRows : true,
		pager : "#pager_list_5",
		viewrecords : true,
		caption : "应用平台列表(sub_code=)",
		onSelectRow : function(rowid)
		{
			var sel_sub_id = $('#table_list_5').jqGrid('getRowData',rowid).MAIN_ID;
			get_T01_sel_t_sub_user_ByPID(sel_sub_id);
		}
		}).trigger("reloadGrid");
	}
	layer.close(ly_index);
}

// 获取返回值
function get_T01_sel_t_proc_return_ByPID()
{
	ly_index = layer.load();
	$.ajax({
	type : "POST",
	async : false,
	url : baseUrl,
	data : {
	param_name : "T01_sel_t_proc_return_ByPID",
	param_value1 : sel_proc_id,
	session_id : session_id
	},
	// 跨域请求的URL
	dataType : "jsonp",
	jsonp : "jsoncallback",
	jsonpCallback : "T01_sel_t_proc_return_ByPID",
	success : function(data2)
	{
	},
	error : function()
	{
		layer.close(ly_index);
		swal({
		title : "告警",
		text : "网络异常或系统故障，请刷新页面！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "刷新",
		closeOnConfirm : false
		},function()
		{
			window.location.reload();
		})
	},
	// 请求完成后的回调函数 (请求成功或失败之后均调用)
	complete : function(XMLHttpRequest,textStatus)
	{
		$("#doing").empty();
		$("#doing").attr("style","display:none");
	}
	});
}

// 返回值结果
function T01_sel_t_proc_return_ByPID(input)
{
	data = input.T01_sel_t_proc_return_ByPID;
	var s_result = "1";
	var error_desc = "";
	for( var key in data[0])
	{
		if(key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if(s_result == "0")
	{
		layer.close(ly_index);
		swal("数据读取失败!","失败原因:" + error_desc,"warning");
		return false;
	}
	$("#table_list_4").jqGrid("clearGridData");
	var total = $("#table_list_4").jqGrid('getGridParam','records');
	if(typeof (total) == "undefined") // || total == 0)
	{
		$("#table_list_4").jqGrid({
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_proc_return,
		editurl : 'clientArray',
		altRows : false,
		pager : "#pager_list_4",
		viewrecords : true,
		caption : "返回值列表"
		});
	}
	else
	{
		$("#table_list_4").jqGrid('setGridParam',{
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_proc_return,
		editurl : 'clientArray',
		altRows : true,
		pager : "#pager_list_4",
		viewrecords : true,
		caption : "返回值列表"
		}).trigger("reloadGrid");
	}
	layer.close(ly_index);
}

// 获取输出参数
function get_T01_sel_t_proc_outparam_ByPID()
{
	ly_index = layer.load();
	$.ajax({
	type : "POST",
	async : false,
	url : baseUrl,
	data : {
	param_name : "T01_sel_t_proc_outparam_ByPID",
	param_value1 : sel_proc_id,
	session_id : session_id
	},
	// 跨域请求的URL
	dataType : "jsonp",
	jsonp : "jsoncallback",
	jsonpCallback : "T01_sel_t_proc_outparam_ByPID",
	success : function(data2)
	{
	},
	error : function()
	{
		layer.close(ly_index);
		swal({
		title : "告警",
		text : "网络异常或系统故障，请刷新页面！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "刷新",
		closeOnConfirm : false
		},function()
		{
			window.location.reload();
		})
	},
	// 请求完成后的回调函数 (请求成功或失败之后均调用)
	complete : function(XMLHttpRequest,textStatus)
	{
		$("#doing").empty();
		$("#doing").attr("style","display:none");
	}
	});
}

// 输出参数结果
function T01_sel_t_proc_outparam_ByPID(input)
{
	data = input.T01_sel_t_proc_outparam_ByPID;
	var s_result = "1";
	var error_desc = "";
	for( var key in data[0])
	{
		if(key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if(s_result == "0")
	{
		layer.close(ly_index);
		swal("数据读取失败!","失败原因:" + error_desc,"warning");
		return false;
	}
	$("#table_list_3").jqGrid("clearGridData");
	var total = $("#table_list_3").jqGrid('getGridParam','records');
	if(typeof (total) == "undefined") // || total == 0)
	{
		$("#table_list_3").jqGrid({
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_proc_outparam,
		editurl : 'clientArray',
		altRows : false,
		pager : "#pager_list_3",
		viewrecords : true,
		caption : "输出参数列表"
		});
	}
	else
	{
		$("#table_list_3").jqGrid('setGridParam',{
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_proc_outparam,
		editurl : 'clientArray',
		altRows : true,
		pager : "#pager_list_3",
		viewrecords : true,
		caption : "输出参数列表"
		}).trigger("reloadGrid");
	}
	layer.close(ly_index);
	get_T01_sel_t_proc_return_ByPID();
}

// 获取输入参数
function get_T01_sel_t_proc_inparam_ByPID(PROC_ID)
{
	ly_index = layer.load();
	$.ajax({
	type : "POST",
	async : false,
	url : baseUrl,
	data : {
	param_name : "T01_sel_t_proc_inparam_ByPID",
	param_value1 : PROC_ID,
	session_id : session_id
	},
	// 跨域请求的URL
	dataType : "jsonp",
	jsonp : "jsoncallback",
	jsonpCallback : "T01_sel_t_proc_inparam_ByPID",
	success : function(data2)
	{
	},
	error : function()
	{
		layer.close(ly_index);
		swal({
		title : "告警",
		text : "网络异常或系统故障，请刷新页面！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "刷新",
		closeOnConfirm : false
		},function()
		{
			window.location.reload();
		})
	},
	// 请求完成后的回调函数 (请求成功或失败之后均调用)
	complete : function(XMLHttpRequest,textStatus)
	{
		$("#doing").empty();
		$("#doing").attr("style","display:none");
	}
	});
}

// 输入参数结果
function T01_sel_t_proc_inparam_ByPID(input)
{
	data = input.T01_sel_t_proc_inparam_ByPID;
	var s_result = "1";
	var error_desc = "";
	for( var key in data[0])
	{
		if(key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if(s_result == "0")
	{
		layer.close(ly_index);
		swal("数据读取失败!","失败原因:" + error_desc,"warning");
		return false;
	}
	$("#table_list_2").jqGrid("clearGridData");
	var total = $("#table_list_2").jqGrid('getGridParam','records');
	if(typeof (total) == "undefined")
	{
		$("#table_list_2").jqGrid({
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_proc_inparam,
		editurl : 'clientArray',
		altRows : true,
		pager : "#pager_list_2",
		viewrecords : true,
		caption : "输入参数列表(param_value1=)"
		}).trigger("reloadGrid");
	}
	else
	{
		$("#table_list_2").jqGrid('setGridParam',{
		data : data,
		datatype : "local",
		height : "auto",
		autowidth : true,
		shrinkToFit : true,
		rowNum : 10,
		rowList : [10,20,30
		],
		colModel : mod_t_proc_inparam,
		editurl : 'clientArray',
		altRows : true,
		pager : "#pager_list_2",
		viewrecords : true,
		caption : "输入参数列表"
		}).trigger("reloadGrid");
	}
	layer.close(ly_index);
	get_T01_sel_t_proc_outparam_ByPID();
}

// 接口名称结果
function T01_sel_t_proc_name(input)
{
	data = input.T01_sel_t_proc_name;
	var s_result = "1";
	var error_desc = "";
	for( var key in data[0])
	{
		if(key == 's_result')
		{
			s_result = data[0].s_result;
			error_desc = data[0].error_desc;
		}
	}
	if(s_result == "0")
	{
		layer.close(ly_index);
		swal("数据读取失败!","失败原因:" + error_desc,"warning");
		return false;
	}
	if(data[0].MAIN_ID < 72)
		data.splice(0,1);
	var total = $("#table_list_1").jqGrid('getGridParam','records');
	if(typeof (total) == "undefined")
	{
		$("#table_list_1")
				.jqGrid({
				data : data,
				datatype : "local",
				height : "auto", // "450",
				autowidth : true,
				shrinkToFit : true,
				rowNum : 10,
				rowList : [10,20,30
				],
				colModel : mod_t_proc_name,
				editurl : 'clientArray',
				altRows : true,
				pager : "#pager_list_1",
				viewrecords : true,
				caption : "接口名称列表(param_name=)&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id= 'debug_t_proc_name' value='接口调试'>",
				onSelectRow : function(rowid)
				{
					sel_proc_id = $('#table_list_1').jqGrid('getRowData',rowid).MAIN_ID;
					get_T01_sel_t_proc_inparam_ByPID(sel_proc_id);
				}
				});

		// 接口调试
		$("#debug_t_proc_name").click(function()
		{
			// iframe层-父子操作
			layer.open({
			type : 2,
			area : ['1100px','600px'
			],
			fixed : false, // 不固定
			maxmin : true,
			content : 'oss_debug_t_porc_name.html'
			});
		});

		// 导航后面，增加增删改查按钮
		$("#table_list_1").jqGrid("navGrid","#pager_list_1",{
		edit : true,
		add : true,
		del : true,
		search : true,
		position : "left",
		cloneToTop : false,
		view : false,
		},{
		beforeShowForm : function()
		{
			$('#DB_ID').attr('readOnly',true);
			$("#DB_ID").attr('disabled',"disabled");
			laydate.render({
			elem : '#CREATE_DATE',
			type : 'datetime'
			});
		},
		closeAfterEdit : true,
		reloadAfterSubmit : true
		},{
			beforeShowForm : function()
			{
				laydate.render({
				elem : '#CREATE_DATE',
				type : 'datetime'
				});
			}
		},{
		top : iTop,
		left : ileft,
		reloadAfterSubmit : true
		},{
			closeAfterSearch : true
		},{
		height : 200,
		reloadAfterSubmit : true
		});
		// 获取应用平台
		get_T01_sel_t_sub_sys();
	}
	else
	{
		$("#table_list_1").jqGrid("clearGridData");
		$("#table_list_1").jqGrid('setGridParam',{
			data : data
		}).trigger("reloadGrid");
	}
	layer.close(ly_index);
}

// 初始化
function init()
{
	iTop = (winHeight - 300) / 2;
	ileft = (winWidth - 300) / 2;
	ly_index = layer.load();
	$.jgrid.defaults.styleUI = "Bootstrap";
	$.ajax({
	type : "POST",
	async : false,
	url : baseUrl,
	data : {
	param_name : "T01_sel_t_proc_name",
	session_id : session_id
	},
	// 跨域请求的URL
	dataType : "jsonp",
	jsonp : "jsoncallback",
	jsonpCallback : "T01_sel_t_proc_name",
	success : function()
	{
	},
	error : function()
	{
		layer.close(ly_index);
		swal({
		title : "告警",
		text : "网络异常或系统故障，请刷新页面！",
		type : "warning",
		showCancelButton : true,
		confirmButtonColor : "#DD6B55",
		confirmButtonText : "刷新",
		closeOnConfirm : false
		},function()
		{
			window.location.reload();
		})
	},
	// 请求完成后的回调函数 (请求成功或失败之后均调用)
	complete : function(XMLHttpRequest,textStatus)
	{
		$("#doing").empty();
		$("#doing").attr("style","display:none");
	}
	});
}

$(document).ready(function()
{
	// init_page();
	getWindowSize();
	get_T01_sel_t_db_config();
	$(window).bind("resize",function()
	{
		var width = $(".jqGrid_wrapper").width();
		$("#table_list_1").setGridWidth(width);
	});
});