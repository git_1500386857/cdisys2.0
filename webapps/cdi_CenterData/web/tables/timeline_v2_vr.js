﻿var iTop = 0;
var ileft = 0;
var ly_index;

function C01_TG73H9(input) {
    data = input.C01_TG73H9;
	var typeDiv = "";
	$.each(data,function(i, obj) {
		var dt_diff = "";
		var dateStart = new Date(new Date(obj.CREATE_DATE).Format('yyyy-MM-dd 00:00:00'));
		var dateEnd = new Date();
		var difValue = (dateEnd - dateStart) / (1000 * 60 * 60 * 24);
		switch(parseInt(difValue))
		{
			case 0:
				dt_diff ="今天";
				break;
			case 1:
				dt_diff ="昨天";
				break;
			case 2:
				dt_diff ="前天";
				break;
			default:
				dt_diff ="三天前";
				break;
		}
		if(i%2 == 0)
		{
			typeDiv = "<div class=\"vertical-timeline-block\">"
					  +"<div class=\"vertical-timeline-icon navy-bg\">"
					  +"<i class=\"fa fa-briefcase\"></i>"
					  +"</div>"
					  +"<div class=\"vertical-timeline-content\">"
					  +"<h2>"+s_decode(obj.INFO_NAME)+"</h2>"
					  +"<p>"+s_decode(obj.INFO_MSG)+"</p>"
					  //+"<a href=\"#" class="btn btn-sm btn-primary">更多信息</a>"
					  +"<span class=\"vertical-date\">"+dt_diff
					  +"<br />"
					  +"<small>"+new Date(obj.CREATE_DATE).Format('yyyy-MM-dd')+"</small></span></div>"
					  +"</div>";
		}
		else
		{
			typeDiv = "<div class=\"vertical-timeline-block\">"
					  +"<div class=\"vertical-timeline-icon blue-bg\">"
					  +"<i class=\"fa fa-file-text\"></i>"
					  +"</div>"
					  +"<div class=\"vertical-timeline-content\">"
					  +"<h2>"+s_decode(obj.INFO_NAME)+"</h2>"
					  +"<p>"+s_decode(obj.INFO_MSG)+"</p>"
					  //+"<a href=\"#" class="btn btn-sm btn-primary">更多信息</a>"
					  +"<span class=\"vertical-date\">"+dt_diff
					  +"<br />"
					  +"<small>"+new Date(obj.CREATE_DATE).Format('yyyy-MM-dd')+"</small></span></div>"
					  +"</div>";
		}
		$("#vertical-timeline").append($(typeDiv));
	});
	
	/*
	$("#lightVersion").click(function (event) {
        event.preventDefault();
        $("#ibox-content").removeClass("ibox-content");
        $("#vertical-timeline").removeClass("dark-timeline");
        $("#vertical-timeline").addClass("light-timeline");
    });
    $("#darkVersion").click(function (event) {
        event.preventDefault();
        $("#ibox-content").addClass("ibox-content");
        $("#vertical-timeline").removeClass("light-timeline");
        $("#vertical-timeline").addClass("dark-timeline");
    });
    $("#leftVersion").click(function (event) {
        event.preventDefault();
        $("#vertical-timeline").toggleClass("center-orientation");
    })*/
	$("#lightVersion").click(function(event){event.preventDefault();$("#ibox-content").removeClass("ibox-content");$("#vertical-timeline").removeClass("dark-timeline");$("#vertical-timeline").addClass("light-timeline")});
	$("#darkVersion").click(function(event){event.preventDefault();$("#ibox-content").addClass("ibox-content");$("#vertical-timeline").removeClass("light-timeline");$("#vertical-timeline").addClass("dark-timeline")});
	$("#leftVersion").click(function(event){event.preventDefault();$("#vertical-timeline").toggleClass("center-orientation")});
	
	$("#leftVersion").click();	
    layer.close(ly_index);
}

//初始化
function init() {
	ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "C01_TG73H9",
            session_id: session_id,
			param_value1:getUrlParam("param_value1")
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "C01_TG73H9",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    getWindowSize();
    init();
});