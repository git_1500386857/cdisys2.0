﻿var iTop = 0;
var ileft = 0;
var sel_page_id = "-1";
var mod_t_page_item;
var ly_index;
//var guid = new GUID();
//alert(guid.newGUID());

//综合应用平台信息
var mod_t_sub_sys = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '综合平台名称',
        name: 'SUB_NAME',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '综合平台授权码',
        name: 'SUB_CODE',
        width: '70px',
        editable: true,
        editrules: true,
        editoptions: {
            defaultValue: new GUID().newGUID()
        }
    }, {
        label: '使用有效期',
        name: 'LIMIT_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: DateAdd("y", 30, new Date()).Format('yyyy-MM-dd hh:mm:ss')
        }
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }
];

//平台用户信息
var mod_t_sub_user = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '授权用户名称',
        name: 'SUB_USERNAME',
        width: '30px',
        editable: true,
        editrules: true
    }, {
        label: '用户授权码',
        name: 'SUB_USERCODE',
        width: '70px',
        editable: true,
        editrules: true,
        editoptions: {
            defaultValue: new GUID().newGUID()
        }
    }, {
        label: '使用有效期',
        name: 'LIMIT_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }
];

var t_page_info = [{
        label: '主键',
        name: 'MAIN_ID',
        width: '30px',
        index: 'MAIN_ID',
        editable: false,
        key: true,
        readOnly: true,
        editrules: {
            required: true
        }
    }, {
        label: '模板编号',
        name: 'PAGE_CODE',
        width: '70px',
        editable: true,
        editrules: true,
        editoptions: {
            defaultValue: new GUID().newGUID()
        }
    }, {
        label: '模板描述',
        name: 'PAGE_TITLE',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '模板url',
        name: 'PAGE_URL',
        width: '70px',
        editable: true,
        editrules: true
    }, {
        label: '系统时间',
        name: 'CREATE_DATE',
        width: '70px',
        editable: true,
        editrules: true,
        readOnly: true,
        formatter: function (value, row) {
            return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
        },
        editoptions: {
            defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
        }
    }, {
        label: '备注',
        name: 'S_DESC',
        width: '70px',
        editable: true,
        editrules: true
    }
    //,{label:'配置js文件',name:'JS_URL',width:'70px',editable:false,editrules:true}
];

//获取接口名称结果
function T01_sel_t_proc_name(input) {
	var data = input.T01_sel_t_proc_name;
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
	
    var options_v2 = {};
    $.each(input.T01_sel_t_proc_name, function (i, obj) {
        options_v2[obj.MAIN_ID.toString()] = obj.DB_ID + "--" + obj.INF_CN_NAME;
    });
    mod_t_page_item = [{
            label: '主键',
            name: 'MAIN_ID',
            width: '30px',
            index: 'MAIN_ID',
            editable: false,
            key: true,
            readOnly: true,
            editrules: {
                required: true
            }
        }, {
            label: '接口名称',
            name: 'PROC_ID',
            width: '50px',
            editable: true,
            readOnly: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editoptions: {
                value: options_v2
            }
        }, {
            label: '明细名称',
            name: 'ITEM_NAME',
            width: '50px',
            editable: true,
            editrules: true
        }, {
            label: '明细类型',
            name: 'ITEM_TYPE',
            width: '50px',
            editable: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editoptions: {
                value: {
                    1: '表格模式',
                    2: '列表模式',
                    3: '曲线图',
                    4: '柱状图'
                }
            },
            formatoptions: {
                value: {
                    1: '表格模式',
                    2: '列表模式',
                    3: '曲线图',
                    4: '柱状图'
                }
            }
        }, {
            label: '显示列数',
            name: 'COLUMN_COUNT',
            width: '50px',
            editable: true,
            editrules: true
        }, {
            label: '备注',
            name: 'S_DESC',
            width: '70px',
            editable: true,
            editrules: true
        }, {
            label: '页面对应输入参数',
            name: 'URL_IN_PARAM',
            width: '50px',
            editable: true,
            editrules: true
        }
    ];

    layer.close(ly_index);
    init();
}

//获取平台用户信息
function get_T01_sel_t_sub_user_ByPID(sel_sub_id) {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_sub_user_ByPID",
            param_value1: sel_sub_id,
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_sub_user_ByPID",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                layer.close(ly_index);
                window.location.reload();
            })
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

//平台用户信息结果
function T01_sel_t_sub_user_ByPID(input) {
    data = input.T01_sel_t_sub_user_ByPID;	
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
	
    var total = $("#table_list_6").jqGrid('getGridParam', 'records')
        $("#table_list_6").jqGrid("clearGridData");
    if (typeof(total) == "undefined") // || total == 0)
    {
        $("#table_list_6").jqGrid({
            data: data,
            datatype: "local",
            height: "auto",
            autowidth: true,
            shrinkToFit: true,
            rowNum: 10,
            rowList: [10, 20, 30],
            colModel: mod_t_sub_user,
            editurl: 'clientArray',
            altRows: false,
            pager: "#pager_list_6",
            viewrecords: true,
            caption: "平台用户列表(sub_usercode=)"
        });
    } else {
        $("#table_list_6").jqGrid('setGridParam', {
            data: data,
            datatype: "local",
            height: "auto",
            autowidth: true,
            shrinkToFit: true,
            rowNum: 10,
            rowList: [10, 20, 30],
            colModel: mod_t_sub_user,
            editurl: 'clientArray',
            altRows: true,
            pager: "#pager_list_6",
            viewrecords: true,
            caption: "平台用户列表(sub_usercode=)"
        }).trigger("reloadGrid");
    }
    layer.close(ly_index);
}

//获取应用平台信息
function get_T01_sel_t_sub_sys() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_sub_sys",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_sub_sys",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            })
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

//应用平台结果
function T01_sel_t_sub_sys(input) {
    data = input.T01_sel_t_sub_sys;	
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
	
    $("#table_list_5").jqGrid("clearGridData");
    var total = $("#table_list_5").jqGrid('getGridParam', 'records')
        if (typeof(total) == "undefined") // || total == 0)
        {
            $("#table_list_5").jqGrid({
                data: data,
                datatype: "local",
                height: "auto",
                autowidth: true,
                shrinkToFit: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                colModel: mod_t_sub_sys,
                editurl: 'clientArray',
                altRows: false,
                pager: "#pager_list_5",
                viewrecords: true,
                caption: "应用平台列表(sub_code=)",
                onSelectRow: function (rowid) {
                    var sel_sub_id = $('#table_list_5').jqGrid('getRowData', rowid).MAIN_ID;
                    get_T01_sel_t_sub_user_ByPID(sel_sub_id);
                }
            });
        } else {
            $("#table_list_5").jqGrid('setGridParam', {
                data: data,
                datatype: "local",
                height: "auto",
                autowidth: true,
                shrinkToFit: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                colModel: mod_t_sub_sys,
                editurl: 'clientArray',
                altRows: true,
                pager: "#pager_list_5",
                viewrecords: true,
                caption: "应用平台列表(sub_code=)",
                onSelectRow: function (rowid) {
                    var sel_sub_id = $('#table_list_5').jqGrid('getRowData', rowid).MAIN_ID;
                    get_T01_sel_t_sub_user_ByPID(sel_sub_id);
                }
            }).trigger("reloadGrid");
        }
        layer.close(ly_index);
}

//获取接口名称
function get_T01_sel_t_proc_name() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_proc_name",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_proc_name",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

//模板明细结果
function T01_sel_t_page_item_ByPID(input) {
    data = input.T01_sel_t_page_item_ByPID;	
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
	
    var total = $("#table_list_2").jqGrid('getGridParam', 'records')
        $("#table_list_2").jqGrid("clearGridData");
    if (typeof(total) == "undefined") // || total == 0)
    {
        $("#table_list_2").jqGrid({
            data: data,
            datatype: "local",
            height: "auto",
            autowidth: true,
            shrinkToFit: true,
            rowNum: 10,
            rowList: [10, 20, 30],
            colModel: mod_t_page_item,
            editurl: 'clientArray',
            altRows: false,
            pager: "#pager_list_2",
            viewrecords: true,
            caption: "模板明细列表"
        });
    } else {
        $("#table_list_2").jqGrid('setGridParam', {
            data: data,
            datatype: "local",
            height: "auto",
            autowidth: true,
            shrinkToFit: true,
            rowNum: 10,
            rowList: [10, 20, 30],
            colModel: mod_t_page_item,
            editurl: 'clientArray',
            altRows: true,
            pager: "#pager_list_2",
            viewrecords: true,
            caption: "模板明细列表"
        }).trigger("reloadGrid");
    }
    layer.close(ly_index);
}

//获取模板明细
function get_T01_sel_t_page_item_ByPID(sel_page_id) {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_page_item_ByPID",
            param_value1: sel_page_id,
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_page_item_ByPID",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                layer.close(ly_index);
                window.location.reload();
            })
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function T01_sel_t_page_info(input) {
    data = input.T01_sel_t_page_info;
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "auto",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: t_page_info,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "APP应用模板列表&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' id= 'debug_t_page_info' value='模板调试'>",
        onSelectRow: function (rowid) {
            sel_page_id = $('#table_list_1').jqGrid('getRowData', rowid).MAIN_ID;
            get_T01_sel_t_page_item_ByPID(sel_page_id);
        }
    });

    //接口调试
    $("#debug_t_page_info").click(function () {
        //iframe层-父子操作
        layer.open({
            type: 2,
            area: ['1100px', '600px'],
            fixed: false, //不固定
            maxmin: true,
            content: 'debug_t_page_info.html'
        });
    });

    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            $('#MAIN_ID').attr('readOnly', true);
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    }, {
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
    layer.close(ly_index);
    //获取应用平台
    get_T01_sel_t_sub_sys();
}

//初始化
function init() {
    ly_index = layer.load();
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_page_info",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_page_info",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    get_T01_sel_t_proc_name();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_page_info(input) {
    data2 = input.T01_ins_t_page_info;
    var s_result = "";
    var error_desc = "";
    for (var key in data2[0]) {
        if (key == 's_result') {
            s_result = data2[0].s_result;
            error_desc = data2[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败!", "失败原因:" + error_desc, "warning");
    } else {
        swal("操作成功!", "", "success");
    }
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_page_info",
            session_id: session_id,
            param_value1: $("#PAGE_CODE").val(),
            param_value2: $("#PAGE_TITLE").val(),
            param_value3: $("#PAGE_URL").val(),
            param_value4: $("#CREATE_DATE").val(),
            param_value5: $("#S_DESC").val(),
            param_value6: ""
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_page_info",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_page_info(input) {
    data2 = input.T01_upd_t_page_info;
    var s_result = "";
    var error_desc = "";
    for (var key in data2[0]) {
        if (key == 's_result') {
            s_result = data2[0].s_result;
            error_desc = data2[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败!", "失败原因:" + error_desc, "warning");
    } else {
        swal("操作成功!", "", "success");
    }
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var updateDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_page_info",
            session_id: session_id,
            param_value1: $("#PAGE_CODE").val(),
            param_value2: $("#PAGE_TITLE").val(),
            param_value3: $("#PAGE_URL").val(),
            param_value4: $("#CREATE_DATE").val(),
            param_value5: $("#S_DESC").val(),
            param_value6: "",
            param_value7: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_page_info",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_page_info(input) {
    data2 = input.T01_del_t_page_info;
    var s_result = "";
    var error_desc = "";
    for (var key in data2[0]) {
        if (key == 's_result') {
            s_result = data2[0].s_result;
            error_desc = data2[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败!", "失败原因:" + error_desc, "warning");
    } else {
        swal("操作成功!", "", "success");
    }
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
    //重新设置jqgrid大小尺寸
    //getWindowSize();
    /*
    $(" #delmodgrid-table").css({
    "top": iTop,
    "left": ileft,
    "height": "140px",
    "width": "390px"
    });*/
}

var delDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_page_info",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_page_info",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};
