﻿var ly_index;
$(document).ready(function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1", window.parent.document).jqGrid("getGridParam", "selrow");
    if (typeof(rowid) != "undefined") {
        var rowData = $('#table_list_1', window.parent.document).jqGrid('getRowData', rowid);
        $("#param_name").val(rowData.INF_EN_NAME);
    }

    rowid = $("#table_list_5", window.parent.document).jqGrid("getGridParam", "selrow");
    if (typeof(rowid) != "undefined") {
        rowData = $('#table_list_5', window.parent.document).jqGrid('getRowData', rowid);
        $("#sub_code").val(rowData.SUB_CODE);
    }

    rowid = $("#table_list_6", window.parent.document).jqGrid("getGridParam", "selrow");
    if (typeof(rowid) != "undefined") {
        rowData = $('#table_list_6', window.parent.document).jqGrid('getRowData', rowid);
        $("#sub_usercode").val(rowData.SUB_USERCODE);
    }

    var rows = $("#table_list_2", window.parent.document).jqGrid("getRowData");
    for (var i = 1; i <= rows.length; i++) {
        var input_prarm_value = "<div class=\"form-group\"><label class='col-sm-3 control-label'>输入参数" + i.toString() + "(param_value" + i.toString() + "=)</label>"
             + "<div class='col-sm-8'>"
             + "<input id='param_value" + i.toString() + "' placeholder='请输入" + rows[i - 1].PARAM_CN_NAME + "' name='param_value" + i.toString() + "' type='text' class='form-control' >"
             + "</div></div>";
        $("#param_values").append(input_prarm_value);
    }

    $("#btnDebug").click(function () {
        $("#debug_result").val("");
        ly_index = layer.load();
        var show_url = basePath+contextPath + "/getdata.jsp?sub_code=" + $("#sub_code").val() + "&sub_usercode=" + $("#sub_usercode").val()+"&session_id="+session_id;
        var rooturl = show_url + "&param_name=" + $("#param_name").val();
        var rows = $("#table_list_2", window.parent.document).jqGrid("getRowData");
        var source_code = "<script type=\"text/javascript\">\r\n"
             + "	$.ajax({\r\n"
             + "		type: \"POST\",\r\n"
             + "		async: false,\r\n"
             + "		url: \"" + show_url + "\",\r\n"
             + "		data:{param_name:\"" + $("#param_name").val() + "\"\r\n";
        //+"			,sub_code:\""+$("#sub_code").val()+"\"\r\n"
        //+"			,sub_usercode:\""+$("#sub_usercode").val()+"\"\r\n";
        for (var i = 1; i <= rows.length; i++) {
            rooturl += "&param_value" + i.toString() + "=" + $("#param_value" + i.toString()).val();
            source_code += "			,param_value" + i.toString() + ":\"" + $("#param_value" + i.toString()).val() + "\"\r\n"
        }
        source_code += "},\r\n"
         + "		//跨域请求的URL\r\n"
         + "		dataType: \"jsonp\",\r\n"
         + "		jsonp: \"jsoncallback\",\r\n"
         + "		jsonpCallback: \"data_result\",\r\n"
         + "		success: function(data) {\r\n"
         + "		},\r\n"
         + "		error: function() {\r\n"
         + "		if(confirm(\"网络故障，请刷新网络\"))\r\n"
         + "			window.location.reload();\r\n"
         + "		},\r\n"
         + "		// 请求完成后的回调函数 (请求成功或失败之后均调用)\r\n"
         + "		complete: function(XMLHttpRequest, textStatus) {\r\n"
         + "		}\r\n"
         + "	});\r\n"
         + "\r\n"
         + "	function data_result(input_data)\r\n"
         + "	{\r\n"
         + "		//alert(input_data);\r\n"
         + "		alert(JSON.stringify(input_data));\r\n"
         + "	}\r\n"
         + "</script>";
        $("#t_proc_name_url").val(rooturl);
        $("#view_source").val(source_code);

        $.ajax({
            type: "POST",
            async: false,
            url: rooturl,
            data: {},
            //跨域请求的URL
            dataType: "jsonp",
            jsonp: "jsoncallback",
            jsonpCallback: "s_debug_result",
            success: function () {},
            error: function () {
                layer.close(ly_index);
                swal({
                    title: "告警",
                    text: "网络异常或系统故障，请刷新页面！",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "刷新",
                    closeOnConfirm: false
                },
                    function () {
                    window.location.reload();
                });
            },
            // 请求完成后的回调函数 (请求成功或失败之后均调用)
            complete: function (XMLHttpRequest, textStatus) {}
        });
    });
    layer.close(ly_index);
    return false;
});

function s_debug_result(input) {
    $("#debug_result").val(JSON.stringify(input));
    layer.close(ly_index);
}