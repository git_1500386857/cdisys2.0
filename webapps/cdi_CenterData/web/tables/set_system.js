﻿var ly_index;
var sope_type = 0;

function init() {
    ly_index = layer.load();

    $("#btnGetIP").click(function () {
        ly_index = layer.load();
        sope_type = 1;
        set_ip(1);
    });
    
    $("#btnSetIP").click(function () {
        ly_index = layer.load();
        sope_type = 2;
        set_ip(2);
    });
    
    $("#btnGetTIME").click(function () {
        
    });
    
    $("#btnGetTIME").click(function () {
        
    }); 
    
    layer.close(ly_index);
    return false;
}

function set_ip(ope_type)
{
	var sys_ck = $('input:radio[name="system_check"]:checked').val();
    param_name = "";
    if(sys_ck == "1")
    	param_name = "CHANGE_IP_WIN";
    else
    	param_name = "CHANGE_IP_LINUX";
    
    $.ajax({
    	type: "POST",
        async: false,
        url: baseUrl,
        data: {
        	param_name: param_name,
            session_id: session_id,
            param_value1: $("#ip_param_value1").val(),
            param_value2: $("#ip_param_value2").val(),
            param_value3: $("#ip_param_value3").val(),
            param_value4: $("#ip_param_value4").val(),
            param_value5: $("#ip_param_value5").val(),
            param_value6: ope_type
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: param_name,
        success: function () {;
            layer.close(ly_index);
        },
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {}
    });
}

function CHANGE_IP_LINUX(input) {
	data = input.CHANGE_IP_LINUX;
    var s_result = "";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败！", "失败原因:" + s_decode(error_desc), "warning");
    } else {
    	if(sope_type == 1)
    	{
    		$("#ip_param_value1").val(data[0].ip);
    		$("#ip_param_value2").val(data[0].netmask);
    		$("#ip_param_value3").val(data[0].gateway);
    		$("#ip_param_value4").val(data[0].dns);
    		$("#mac_addr").val(data[0].mac);
    	}
    	else if(sope_type == 2)
    		swal("操作成功！", "您已经成功操作IP", "success");
    }
	
    layer.close(ly_index);
}

function CHANGE_IP_WIN(input) {
	data = input.CHANGE_IP_WIN;
    var s_result = "";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败！", "失败原因:" + s_decode(error_desc), "warning");
    } else {
    	if(sope_type == 1)
    	{
    		$("#ip_param_value1").val(data[0].ip);
    		$("#ip_param_value2").val(data[0].netmask);
    		$("#ip_param_value3").val(data[0].gateway);
    		$("#ip_param_value4").val(data[0].dns);
    		$("#mac_addr").val(data[0].mac);
    	}
    	else if(sope_type == 2)
    		swal("操作成功！", "您已经成功操作IP", "success");
    }
	
    layer.close(ly_index);
}

$(document).ready(function () {
    init_page();
    init();
});
