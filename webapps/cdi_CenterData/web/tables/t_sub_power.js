﻿var iTop = 0;
var ileft = 0;
var ly_index;

//综合应用平台信息
var mod_t_sub_sys;
var options = {};
var options_v2 = {};

//获取应用平台结果
function T01_sel_t_sub_sys(input) {
	var data = input.T01_sel_t_sub_sys;
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
    $.each(input.T01_sel_t_sub_sys, function (i, obj) {
        options[obj.MAIN_ID.toString()] = obj.SUB_NAME;
    });
    layer.close(ly_index);
    get_T01_sel_t_proc_name();
}

//获取接口名称结果
function T01_sel_t_proc_name(input) {
	var data = input.T01_sel_t_proc_name;
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
    $.each(input.T01_sel_t_proc_name, function (i, obj) {
        options_v2[obj.MAIN_ID.toString()] = obj.DB_ID + "--" + obj.INF_CN_NAME;
    });

    mod_t_sub_sys = [{
            label: '主键',
            name: 'MAIN_ID',
            width: '30px',
            index: 'MAIN_ID',
            editable: false,
            key: true,
            readOnly: true,
            editrules: {
                required: true
            }
        }, {
            label: '综合应用平台',
            name: 'SUB_ID',
            width: '50px',
            editable: true,
            readOnly: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editoptions: {
                value: options
            }
        }, {
            label: '接口名称主键',
            name: 'PROC_ID',
            width: '70px',
            editable: true,
            readOnly: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editoptions: {
                value: options_v2
            }
        }, {
            label: '使用有效期',
            name: 'LIMIT_DATE',
            width: '50px',
            editable: true,
            editrules: true,
            readOnly: true,
            readOnly: true,
            formatter: function (value, row) {
                return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
            },
            editoptions: {
                defaultValue: DateAdd("y", 30, new Date()).Format('yyyy-MM-dd hh:mm:ss')
            }
        }, {
            label: '已调用接口次数',
            name: 'USE_LIMIT',
            width: '50px',
            editable: true,
            editrules: true,
            editoptions: {
                defaultValue: "0"
            }
        }, {
            label: '调用次数限制',
            name: 'LIMIT_NUMBER',
            width: '40px',
            editable: true,
            editrules: true,
            editoptions: {
                defaultValue: 10000
            }
        }, {
            label: '次数限制类型',
            name: 'LIMIT_TAG',
            width: '50px',
            editable: true,
            editrules: true,
            edittype: 'select',
            formatter: 'select',
            editable: true,
            editrules: true,
            editoptions: {
                value: {
                    1: '年为基础',
                    2: '月为基础',
                    3: '日为基础'
                },
                defaultValue: 3
            },
            formatoptions: {
                value: {
                    1: '年为基础',
                    2: '月为基础',
                    3: '日为基础'
                }
            }
        }, {
            label: '第一次调用时间',
            name: 'FIRST_DATE',
            width: '60px',
            editable: true,
            editrules: true,
            readOnly: true,
            formatter: function (value, row) {
                return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
            },
            editoptions: {
                defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
            }
        }, {
            label: '备注',
            name: 'S_DESC',
            width: '50px',
            editable: true,
            editrules: true
        }, {
            label: '系统时间',
            name: 'CREATE_DATE',
            width: '50px',
            editable: true,
            editrules: true,
            readOnly: true,
            formatter: function (value, row) {
                return new Date(value).Format('yyyy-MM-dd hh:mm:ss');
            },
            editoptions: {
                defaultValue: new Date().Format('yyyy-MM-dd hh:mm:ss')
            }
        }
    ];
    layer.close(ly_index);
    init();
}

//获取接口名称
function get_T01_sel_t_proc_name() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_proc_name",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_proc_name",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

//获取应用平台名称
function get_T01_sel_t_sub_sys() {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_sub_sys",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_sub_sys",
        success: function (data2) {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

function T01_sel_t_sub_power(input) {
    data = input.T01_sel_t_sub_power;
	var s_result = "1";
    var error_desc = "";
    for (var key in data[0]) {
        if (key == 's_result') {
            s_result = data[0].s_result;
            error_desc = data[0].error_desc;
        }
    }
    if (s_result == "0") {
		layer.close(ly_index);
        swal("数据读取失败!", "失败原因:" + error_desc, "warning");
		return false;
    }
    $("#table_list_1").jqGrid({
        data: data,
        datatype: "local",
        height: "450",
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colModel: mod_t_sub_sys,
        editurl: 'clientArray',
        altRows: true,
        pager: "#pager_list_1",
        viewrecords: true,
        caption: "应用平台授权列表"
    });
    //导航后面，增加增删改查按钮
    $("#table_list_1").jqGrid("navGrid", "#pager_list_1", {
        edit: true,
        add: true,
        del: true,
        search: true,
        view: false,
        position: "left",
        cloneToTop: false
    }, {
        beforeShowForm: function () {
            $('#PROC_ID').attr('readOnly', true);
            $("#SUB_ID").attr('disabled', "disabled");
            $("#PROC_ID").attr('disabled', "disabled");
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
            laydate.render({
                elem: '#LIMIT_DATE',
                type: 'datetime'
            });
            laydate.render({
                elem: '#FIRST_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: updateDbByMainId,
        closeAfterEdit: true
    }, {
        beforeShowForm: function () {
            laydate.render({
                elem: '#CREATE_DATE',
                type: 'datetime'
            });
            laydate.render({
                elem: '#LIMIT_DATE',
                type: 'datetime'
            });
            laydate.render({
                elem: '#FIRST_DATE',
                type: 'datetime'
            });
        },
        onclickSubmit: insertDb,
        closeAfterAdd: true
    }, {
        top: iTop,
        left: ileft,
        onclickSubmit: delDbByMainId
    }, {
        closeAfterSearch: true
    }, {
        height: 200,
        reloadAfterSubmit: true
    });
    layer.close(ly_index);
}

//初始化
function init() {
    iTop = (winHeight - 300) / 2;
    ileft = (winWidth - 300) / 2;
    ly_index = layer.load();
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_sel_t_sub_power",
            session_id: session_id
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_sel_t_sub_power",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
}

$(document).ready(function () {
    init_page();
    getWindowSize();
    get_T01_sel_t_sub_sys();
    $(window).bind("resize",
        function () {
        var width = $(".jqGrid_wrapper").width();
        $("#table_list_1").setGridWidth(width);
    });
});

function T01_ins_t_sub_power(input) {
    data2 = input.T01_ins_t_sub_power;
    var s_result = "";
    var error_desc = "";
    for (var key in data2[0]) {
        if (key == 's_result') {
            s_result = data2[0].s_result;
            error_desc = data2[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败!", "失败原因:" + error_desc, "warning");
    } else {
        swal("操作成功!", "", "success");
    }
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var insertDb = function () {
    ly_index = layer.load();
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_ins_t_sub_power",
            session_id: session_id,
            param_value1: $("#SUB_ID").val(),
            param_value2: $("#PROC_ID").val(),
            param_value3: new Date($("#LIMIT_DATE").val()).Format('yyyy-MM-dd'),
            param_value4: $("#USE_LIMIT").val(),
            param_value5: $("#LIMIT_NUMBER").val(),
            param_value6: $("#LIMIT_TAG").val(),
            param_value7: new Date($("#FIRST_DATE").val()).Format('yyyy-MM-dd'),
            param_value8: $("#S_DESC").val(),
            param_value9: $("#CREATE_DATE").val()
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_ins_t_sub_power",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_upd_t_sub_power(input) {
    data2 = input.T01_upd_t_sub_power;
    var s_result = "";
    var error_desc = "";
    for (var key in data2[0]) {
        if (key == 's_result') {
            s_result = data2[0].s_result;
            error_desc = data2[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败!", "失败原因:" + error_desc, "warning");
    } else {
        swal("操作成功!", "", "success");
    }
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);
}

var updateDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_upd_t_sub_power",
            session_id: session_id,
            param_value1: $("#PROC_ID").val(),
            param_value2: new Date($("#LIMIT_DATE").val()).Format('yyyy-MM-dd'),
            param_value3: $("#USE_LIMIT").val(),
            param_value4: $("#LIMIT_NUMBER").val(),
            param_value5: $("#LIMIT_TAG").val(),
            param_value6: new Date($("#FIRST_DATE").val()).Format('yyyy-MM-dd'),
            param_value7: $("#S_DESC").val(),
            param_value8: $("#CREATE_DATE").val(),
            param_value9: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_upd_t_sub_power",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};

function T01_del_t_sub_power(input) {
    data2 = input.T01_del_t_sub_power;
    var s_result = "";
    var error_desc = "";
    for (var key in data2[0]) {
        if (key == 's_result') {
            s_result = data2[0].s_result;
            error_desc = data2[0].error_desc;
        }
    }
    if (s_result != "1") {
        swal("操作失败!", "失败原因:" + error_desc, "warning");
    } else {
        swal("操作成功!", "", "success");
    }
    $("#table_list_1").trigger("reloadGrid");
    layer.close(ly_index);

    //重新设置jqgrid大小尺寸
    //getWindowSize();
    /*
    $(" #delmodgrid-table").css({
    "top": iTop,
    "left": ileft,
    "height": "140px",
    "width": "390px"
    });*/
}

var delDbByMainId = function () {
    ly_index = layer.load();
    var rowid = $("#table_list_1").jqGrid("getGridParam", "selrow");
    var rowData = $('#table_list_1').jqGrid('getRowData', rowid);
    $.ajax({
        type: "POST",
        async: false,
        url: baseUrl,
        data: {
            param_name: "T01_del_t_sub_power",
            session_id: session_id,
            param_value1: rowData.MAIN_ID
        },
        //跨域请求的URL
        dataType: "jsonp",
        jsonp: "jsoncallback",
        jsonpCallback: "T01_del_t_sub_power",
        success: function () {},
        error: function () {
            layer.close(ly_index);
            swal({
                title: "告警",
                text: "网络异常或系统故障，请刷新页面！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "刷新",
                closeOnConfirm: false
            },
                function () {
                window.location.reload();
            });
        },
        // 请求完成后的回调函数 (请求成功或失败之后均调用)
        complete: function (XMLHttpRequest, textStatus) {
            $("#doing").empty();
            $("#doing").attr("style", "display:none");
        }
    });
};
