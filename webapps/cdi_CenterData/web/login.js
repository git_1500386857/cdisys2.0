$(document).ready(function() {
   $("#btnLogin").click(function(){
		$.ajax({
			type: "POST",
			async: false,
			url: baseUrl,
			data: {
				param_name: "T01_F7G83H",
				param_value1: $("#login_id").val(),
				param_value2: $("#login_pwd").val()
			},
			//跨域请求的URL
			dataType: "jsonp",
			jsonp: "jsoncallback",
			jsonpCallback: "T01_F7G83H",
			success: function(input) {
				data2 = input.T01_F7G83H;
				var s_result = "";
				var error_desc = "";
				for (var key in data2[0]) {
					if (key == 's_result') 
					{
						s_result = data2[0].s_result;
						error_desc = data2[0].error_desc;
					}
				}
				if (s_result != "1") {
					swal("登录失败！", "登录失败！:"+error_desc, "warning");
				}
				else{
					localStorage.setItem('session_id', data2[0].session_id);				
					localStorage.setItem('login_id', data2[0].MAIN_ID);			
					//localStorage.setItem('nickname', data2[0].nickname);	
					//localStorage.setItem('rid', data2[0].rid);
					window.location.href = "index.html";
				}
			},
			error: function() {
				swal({
					title: "告警",
					text: "网络异常或系统故障，请刷新页面！",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "刷新",
					closeOnConfirm: false
				},
				function() {
					window.location.reload();
				})
			},
			// 请求完成后的回调函数 (请求成功或失败之后均调用)
			complete: function(XMLHttpRequest, textStatus) {
				$("#doing").empty();
				$("#doing").attr("style", "display:none");
			}
		});
   })
});