<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="../plugins/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="../plugins/ajaxup/ajaxfileupload.js"></script>
<title>企业文件上传</title>
</head>
<body>
    <form action="/devcompanyFileUpload" method="post" enctype="multipart/form-data">
      <input type="text" name="main_id"  id="main_id"  readonly="readonly"/>
      <input type="file"  name="file"  id="file" />
      <input type="submit" value="上传文件" />
    </form>
</body>
<script type="text/javascript">
  var main_id = "${main_id}";
  $("#main_id").val(main_id);
  </script>
</html>