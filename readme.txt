本系统工具作为关系型数据库接口配置管理工具，可通过图形化界面配置各类数据接口，无需编写任何后台java代码，实现可视化配置即结果模式，易于扩展；可将程序开发人员的重复接口定义工作从繁琐的重复性后台代码中解脱出来，让程序开发人员更专注于数据业务的分析和理解。使用该系统人员需要对数据库具有一定应用基础知识，熟练掌握各类数据库SQL语句及存储过程编写能力。
工具共分为两部分：
cdi_demo为接口图形化定义界面功能。可对接口数据源、接口功能定义、输入参数、输出参数、返回值等各类功能进行界面管理操作。
cdi_CenterData为接口数据服务功能，提供预定义接口数据交互功能。
接口返回格式为json格式

在线演示地址：
https://124.71.168.14/cdi_demo/
账号密码：
guest/1
下载地址：
http://124.71.168.14/cdi_demo/CDIsys_v2.0(20210423).rar